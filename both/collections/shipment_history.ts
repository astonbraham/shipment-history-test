this.ShipmentHistory = new Mongo.Collection("shipment_history");

this.ShipmentHistory.userCanInsert = function(userId, doc) {
	return true;
};

this.ShipmentHistory.userCanUpdate = function(userId, doc) {
	return true;
};

this.ShipmentHistory.userCanRemove = function(userId, doc) {
	return true;
};
