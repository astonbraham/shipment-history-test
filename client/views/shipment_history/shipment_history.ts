var previously_mapped_event_day = "";

Template.ShipmentHistory.onCreated(function() {
	
});

Template.ShipmentHistory.onDestroyed(function() {
	
});

Template.ShipmentHistory.onRendered(function() {
	
	Meteor.defer(function() {
		globalOnRendered();
		$("input[autofocus]").focus();
	});
});

Template.ShipmentHistory.events({
	
});

Template.ShipmentHistory.helpers({
	
});


Template.SingleEventView.helpers({


		isValidEventComment(comment)
		{
			if(comment && String(comment).valueOf() !== "null")
			{
				return true
			}
			return false
		},

		hasArrived(status)
		{
			if(status && String(status).valueOf()  === "ARRIVED")
			{
				return true;
			}
			return false;
		},

		isDelivered(status)
		{
			if(status  && String(status).valueOf()  === "DELIVERED")
			{
				return true;
			}
			return false;
		},
		event_status(isDelayed)
		{
			if(isDelayed)
			{
					return 'delayed';
			}
		},

		get_event_formatted_date : function (date_element)
		{
			var formatted_date = moment(date_element).format('MMMM Do');
			if(previously_mapped_event_day !== formatted_date) {
				previously_mapped_event_day = formatted_date;
				return formatted_date;
			}
			return ""

		},

		get_event_formatted_time : function (date_element)
		{
			return moment(date_element).format('h:mma');
		}

});
