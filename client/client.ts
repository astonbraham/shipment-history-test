this.App = {};
this.Helpers = {};

this.globalOnRendered = function() {
	
};

Meteor.startup(function() {
	
});


Helpers.menuItemClass = function(routeName, params) {
	return menuItemClass(routeName, params);
};


Helpers.isPrivateZone = function() {
	return getCurrentZone() == "private";
};

Helpers.isPublicZone = function() {
	return getCurrentZone() == "public";
};

Helpers.isFreeZone = function() {
	return getCurrentZone() == "free";
};


Helpers.randomString = function(strLen) {
	return Random.id(strLen);
};


_.each(Helpers, function (helper, key) {
	Handlebars.registerHelper(key, helper);
});
