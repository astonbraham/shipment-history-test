Meteor.methods({
	"shipmentHistoryInsert": function(data) {
		if(!ShipmentHistory.userCanInsert(this.userId, data)) {
			throw new Meteor.Error(403, "Forbidden.");
		}

		return ShipmentHistory.insert(data);
	},

	"shipmentHistoryUpdate": function(id, data) {
		var doc = ShipmentHistory.findOne({ _id: id });
		if(!ShipmentHistory.userCanUpdate(this.userId, doc)) {
			throw new Meteor.Error(403, "Forbidden.");
		}

		ShipmentHistory.update({ _id: id }, { $set: data });
	},

	"shipmentHistoryRemove": function(id) {
		var doc = ShipmentHistory.findOne({ _id: id });
		if(!ShipmentHistory.userCanRemove(this.userId, doc)) {
			throw new Meteor.Error(403, "Forbidden.");
		}

		ShipmentHistory.remove({ _id: id });
	}
});
